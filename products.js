
let productsOg = [];
let productsMod = [];
// /////////////////////////////////////// RECOGER ARRAY PRODUCTOS DE LA API ///////////////////////////////////////
getProducts();
async function getProducts() {
    const res = await fetch('http://localhost:3000/products');
    const products = await res.json();
    productsOg = products;
    productsMod = products;
    printProducts(productsMod);
    // ratingStars(productsMod);
}
// ///////////////////////////////////// SELECCIONAR EL DIV CONTENEDOR DE HTML ///////////////////////////////////
const productContainer$$ = document.querySelector('[data-function="products"]');
// //////////////////////////////////// PINTAR CADA PRODUCTO DEL ARRAY //////////////////////////////////////
function printProducts(products) {
    // console.log(products)
    for (const product of products) {
        createProduct(product)
    }
}

const myGrid = document.querySelector('[data-function="grid"]')
const myLista = document.querySelector('[data-function="list"]')

function createProduct(product) {
    const myDiv$$ = document.createElement('Div');
    const divImage = document.createElement('div')
    const divText = document.createElement('div')
    const divBR = document.createElement('div')
    const imgProduct = document.createElement('img')
    const titleProduct = document.createElement('h3')
    const descriptionProduct = document.createElement('p')
    const priceProduct = document.createElement('span')
    const ratingProduct = document.createElement('span')
    const createButton = document.createElement('button')

    editButton(createButton)

    imgProduct.setAttribute('src', product.image)

    titleProduct.classList = 'cart__tittle'
    imgProduct.classList.add('imgjava')
    divImage.classList = "innerdiv"
    divText.classList = "cart__cardtext"
    priceProduct.classList = "cart__spanjava"
    divBR.classList = "cart__lastsection"
    myDiv$$.setAttribute('class', 'cart col-md-4 col-lg-3')
    createButton.classList = 'stylebtn btn btn-outline-primary'

    myLista.addEventListener('click', function () {
        myDiv$$.classList.remove('col-lg-3')
        myDiv$$.classList.remove('col-md-4')
        myDiv$$.classList.add('col-12')

    })
    myGrid.addEventListener('click', function () {
        myDiv$$.classList.remove('col-12')
        myDiv$$.classList.add('col-md-4')
        myDiv$$.classList.add('col-lg-3')

    })



    titleProduct.innerHTML = product.name
    descriptionProduct.innerHTML = product.description
    priceProduct.innerHTML = `${product.price} $`
    ratingProduct.innerHTML = product.stars
    if (product.stars <= 1) {
        ratingProduct.innerHTML = `<img src="icon/starfill.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        ${product.stars}
        
        `
    }
    if (product.stars <= 2 && product.stars > 1) {
        ratingProduct.innerHTML = `<img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        ${product.stars}
        
        `
    }
    if (product.stars <= 3 && product.stars > 2) {
        ratingProduct.innerHTML = `<img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/star.png" width="10px">
        <img src="icon/star.png" width="10px">
        ${product.stars}
        
        `
    }
    if (product.stars <= 4 && product.stars > 3) {
        ratingProduct.innerHTML = `<img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/star.png" width="10px">
        ${product.stars}
        
        `
    }
    if (product.stars <= 5 && product.stars > 4) {
        ratingProduct.innerHTML = `<img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        <img src="icon/starfill.png" width="10px">
        ${product.stars}
        
        `
    }

    createButton.innerHTML = 'Editar'

    divImage.appendChild(imgProduct)
    divText.appendChild(titleProduct)
    divText.appendChild(descriptionProduct)
    divBR.appendChild(ratingProduct)
    divBR.appendChild(createButton)

    myDiv$$.appendChild(divImage)
    myDiv$$.appendChild(divText)
    myDiv$$.appendChild(priceProduct)
    myDiv$$.appendChild(divBR)


    productContainer$$.appendChild(myDiv$$)

}

// /////////////////////////////////////////////// FILTRAR PRODUCTOS ////////////////////////////////////

let inputValue = document.getElementById('search-input').addEventListener('input', (event) => filterProducts(event));

function filterProducts(event) {

    let filteredProducts = productsMod.filter(product => product.name.toLowerCase().includes(event.target.value.toLowerCase()))
    productContainer$$.innerHTML = ''
    for (const product of filteredProducts) {
        createProduct(product)
    }
}

///////////////////////////////////////////////// BOTON EDIT /////////////////////////////////////////////


function editButton(btn) {

    btn.addEventListener('click', function () {
        // window.location.href = 'form.html'
        // console.log(btn.id)
    })
}



// ////////////


